package reverse;

import java.util.*;
public class reverse_array
{
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] numbers = new int[5];

        System.out.print("Please enter number : ");
        for (int i = 0; i < numbers.length; i++){
            numbers[i] = kb.nextInt();
        }
        
        //print reverse array back -> front 
        System.out.print("Output Reverse Array : ");
             for(int i=numbers.length-1;i>=0;i--)  //.length-1 = last number
             System.out.print(numbers[i] + " ");
    }
}